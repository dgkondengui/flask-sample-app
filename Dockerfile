FROM python:alpine3.7
MAINTAINER Gerold "dgkondengui@gmail.com" # Change the name and email address
COPY app.py /app/
WORKDIR /app
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
EXPOSE 80
CMD ["python", "app.py"]
